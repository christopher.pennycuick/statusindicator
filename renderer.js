// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.
const { ipcRenderer } = require('electron');
const App = require('./src/App');

ipcRenderer.on(App.EVENT_LED_CHANGE_COLOR, (event, led_color) => {
	app.$store.Led.setColor(led_color);
});
ipcRenderer.on(App.EVENT_LED_STATUS, (event, status) => {
	app.$store.App.data.device_connection = status;
});
ipcRenderer.on(App.EVENT_CHAT_CONNECTION, (event, status) => {
	app.$store.App.data.chat_connection = status;
});
ipcRenderer.on(App.EVENT_CHAT_STATUS, (event, status) => {
	app.$store.App.data.chat_status = status;
});
ipcRenderer.on(App.EVENT_ACTIVITY_CHANGE, (event, status) => {
	app.$store.App.data.activity = status;
});
ipcRenderer.on(App.EVENT_CONFIG_CHANGE, (event, data) => {
	app.$store.Preferences.setPreferences(data);
});
ipcRenderer.on(App.EVENT_MENU_ITEM, (event, item) => {
	location.hash = item;
});

window.onbeforeunload = function () {
	ipcRenderer.send(App.EVENT_WINDOW_UNLOAD);
};
app.$store.Preferences.save.listen(data => {
	ipcRenderer.send(App.EVENT_CONFIG_CHANGE, data);
});

ipcRenderer.send(App.EVENT_APP_STATE);

// config.write('led.brightness', 0.2);
// config.write('mattermost.url', 'https://mm.codebots.com');
// config.write('mattermost.token', '9y4bd71x8tfbie1ut318f6nw5r');
