# StatusIndicator (BlinkStick)

## Install

Clone respository

```bash
npm start
```

*Note:* At time of writing, I needed to use a local clone of BlinkStick to use the latest version of node-hid.

## To Run

```bash
npm restart
```

## License

[CC0 1.0 (Public Domain)](LICENSE.md)
