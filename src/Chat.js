
const Config = require('./Config');
const Events = require('./Util/Events');
const State = require('./Util/State');

const MatterMost = require('./Chat/MatterMost');

class Chat extends Events {

	constructor(config) {
		super();

		this._config = config;

		this.state = new State();
		this.state.define(Chat.STATE_CONNECTION, Chat.CONNECTION_DISCONNECTED, 'Disconnected');
		this.state.define(Chat.STATE_CONNECTION, Chat.CONNECTION_CONNECTED, 'Connected');
		this.state.define(Chat.STATE_STATUS, Chat.STATUS_OFFLINE, 'Offline');
		this.state.define(Chat.STATE_STATUS, Chat.STATUS_ONLINE, 'Online');
		this.state.define(Chat.STATE_STATUS, Chat.STATUS_AWAY, 'Away');
		this.state.define(Chat.STATE_STATUS, Chat.STATUS_DND, 'Do Not Disturb');
	}

	connect() {
		
	}

	disconnect() {

	}

}

Chat.PROVIDER_NONE = 'Provider.None';
Chat.PROVIDER_MATTERMOST = 'Provider.MatterMost';

Chat.EVENT_CONNECTION = 'connection.*';
Chat.EVENT_CONNECTION_CONNECTED = 'connection.connected';
Chat.EVENT_CONNECTION_DISCONNECTED = 'connection.disconnected';
Chat.EVENT_STATUS_CHANGE = 'status.change';

Chat.STATE_CONNECTION = 'connection';
Chat.STATE_STATUS = 'status';

Chat.CONNECTION_CONNECTED = 'connected';
Chat.CONNECTION_DISCONNECTED = 'disconnected';

Chat.STATUS_ONLINE = 'online';
Chat.STATUS_AWAY = 'away';
Chat.STATUS_DND = 'dnd';
Chat.STATUS_OFFLINE = 'offline';

class ChatMatterMost extends Chat {

	constructor(config) {
		super(config);

		this._mattermost = new MatterMost(config.read('chat.mattermost.url'), config.read('chat.mattermost.token'));

		this._mattermost.on(MatterMost.EVENT_CONNECTION_AUTHENTICATED, this._onConnectionAuthenticated.bind(this));
		this._mattermost.on(MatterMost.EVENT_CONNECTION_DISCONNECTED, this._onConnectionDisconnected.bind(this));
		this._mattermost.on(MatterMost.EVENT_MESSAGE(MatterMost.MESSAGE_EVENT_STATUS_CHANGE), this._onMessageStatusChange.bind(this));

		this._mapStatus = {};
		this._mapStatus[MatterMost.STATUS_ONLINE] = Chat.STATUS_ONLINE;
		this._mapStatus[MatterMost.STATUS_AWAY] = Chat.STATUS_AWAY;
		this._mapStatus[MatterMost.STATUS_DND] = Chat.STATUS_DND;
		this._mapStatus[MatterMost.STATUS_OFFLINE] = Chat.STATUS_OFFLINE;
	}

	connect() {
		this._mattermost.connect();
	}
	disconnect() {
		this._mattermost.disconnect();
	}

	_onConnectionAuthenticated() {
		this.state[Chat.STATE_CONNECTION] = Chat.CONNECTION_CONNECTED;

		this._getCurrentStatus();
	}
	_onConnectionDisconnected() {
		this.state[Chat.STATE_CONNECTION] = Chat.CONNECTION_DISCONNECTED;
		this.state[Chat.STATE_STATUS] = Chat.STATUS_OFFLINE;
	}
	_onMessageStatusChange(payload) {
		if (payload.broadcast.user_id != this._mattermost.userId) {
			return;
		}

		this.state[Chat.STATE_STATUS] = this._mapStatusToChatStatus(payload.data.status);
	}

	_getCurrentStatus() {
		let userId = this._mattermost.userId;
		this._mattermost.send(MatterMost.MESSAGE_ACTION_GET_STATUSES_BY_IDS, {user_ids: [userId]})
			.then(message => {
				this.state[Chat.STATE_STATUS] = this._mapStatusToChatStatus(message.data[userId]);
			});
	}
	_mapStatusToChatStatus(statusMatterMost) {
		if (statusMatterMost in this._mapStatus) {
			return this._mapStatus[statusMatterMost];
		} else {
			return null;
		}
	}

}

class ChatProxy extends Chat {

	constructor (config) {
		super(config);

		config.on(Config.EVENT_UPDATE('chat.*'), () => {
			let chatConfig = config.read('chat');

			this.disconnect();
			this._chat = null;

			this._chat = this._create(chatConfig.provider);
			this._setupEventProxy();

			this.connect();
		});

		this._chat = this._create(config.read('chat.provider'));
		this._setupEventProxy();
	}

	connect() {
		this._chat.connect();
	}
	disconnect() {
		this._chat.disconnect();
	}

	_create (provider) {
		switch (provider) {
			case Chat.PROVIDER_MATTERMOST: {
				return new ChatMatterMost(this._config);
			}

			case Chat.PROVIDER_NONE:
			default: {
				return new Chat(this._config);
			}
		}
	}
	_setupEventProxy() {
		this._chat.on('*', this.trigger.bind(this));
		this._chat.state.on('*', function (key, value) {
			this.state[key] = value;
		}.bind(this));
	}

}

module.exports = {
	Chat,
	Proxy: ChatProxy,
};