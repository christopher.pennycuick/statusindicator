const { ipcMain } = require('electron');

const Config = require("./Config");
const Activity = require("./Activity");
const { Chat, Proxy: ChatProxy } = require("./Chat");
const Led = require("./Led");
const Status = require("./Status");

class App {

	constructor(mainWindow) {
		this.mainWindow = mainWindow;

		this.config = new Config();

		this.activity = new Activity(this.config);
		this.chat = new ChatProxy(this.config);
		this.led = new Led(this.config);
		
		this.status = new Status(this.config, this.activity, this.chat, this.led);

		this._bind();
	}

	_bind() {
		this._bindIPCMain();
		this._bindConfig();
		this._bindLed();
		this._bindChat();
		this._bindActivity();
	}
	_bindIPCMain() {
		ipcMain.on(App.EVENT_WINDOW_UNLOAD, () => {
			this.led.close();
		});
		ipcMain.on(App.EVENT_APP_STATE, () => {
			this.mainWindow.webContents.send(App.EVENT_CONFIG_CHANGE, this.config.data);

			this.mainWindow.webContents.send(App.EVENT_LED_STATUS, this.led.state.name(Led.STATE_CONNECTION));
			this.mainWindow.webContents.send(App.EVENT_LED_CHANGE_COLOR, this.led.rgb);

			this.mainWindow.webContents.send(App.EVENT_CHAT_CONNECTION, this.chat.state.name(Chat.STATE_CONNECTION));
			this.mainWindow.webContents.send(App.EVENT_CHAT_STATUS, this.chat.state.name(Chat.STATE_STATUS));

			this.mainWindow.webContents.send(App.EVENT_ACTIVITY_CHANGE, this._getActivityText());

			this.status.updateLed();
		});
	}
	_bindConfig() {
		ipcMain.on(App.EVENT_CONFIG_CHANGE, (event, config) => {
			this.config.writeAll(config);
		});
	}
	_bindLed() {
		this._proxy(this.led, Led.EVENT_CHANGE_COLOR, App.EVENT_LED_CHANGE_COLOR);
		this._proxy(this.led.state, Led.STATE_CONNECTION, App.EVENT_LED_STATUS, () => {
			return [this.led.state.name(Led.STATE_CONNECTION)];
		});
	}
	_bindChat() {
		this._proxy(this.chat.state, Chat.STATE_CONNECTION, App.EVENT_CHAT_CONNECTION, () => {
			return [this.chat.state.name(Chat.STATE_CONNECTION)];
		});
		this._proxy(this.chat.state, Chat.STATE_STATUS, App.EVENT_CHAT_STATUS, () => {
			return [this.chat.state.name(Chat.STATE_STATUS)];
		});
	}
	_bindActivity() {
		this._proxy(this.activity.state, '*', App.EVENT_ACTIVITY_CHANGE, () => {
			return [this._getActivityText()];
		});
	}
	_getActivityText() {
		return this.activity.state.name(Activity.STATE_ACTIVE) + ' ' + this.activity.state.name(Activity.STATE_ATWORK);
	}

	_proxy(emitter, emitterEvent, appEvent, callback) {
		emitter.on(emitterEvent, (...args) => {
			let result = (callback ? callback.apply(this, args) : args);
			try {
				this.mainWindow.webContents.send(appEvent, ...result);
			} catch (e) {
				
			}
		});
	}

	run() {
		this.chat.connect();
	}
	
}

App.EVENT_WINDOW_UNLOAD = 'WindowUnload';
App.EVENT_APP_STATE = 'AppState';
App.EVENT_LED_CHANGE_COLOR = 'LedChangeColor';
App.EVENT_LED_STATUS = 'LedStatus';
App.EVENT_CHAT_CONNECTION = 'ChatConnection';
App.EVENT_CHAT_STATUS = 'ChatStatus';
App.EVENT_ACTIVITY_CHANGE = 'ActivtyChange';
App.EVENT_CONFIG_CHANGE = 'ConfigChange';
App.EVENT_MENU_ITEM = 'MenuItem';

module.exports = App;
