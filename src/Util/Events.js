class Events {

	constructor () {
		this._events = {};
	}

	on (eventName, callback) {
		if (!(eventName in this._events)) {
			this._events[eventName] = [];
		}
		if (!(callback in this._events[eventName])) {
			this._events[eventName].push(callback);
		}
	}

	once (eventName, callback) {
		this.on(eventName, (...args) => {
			this.off(eventName);
			callback.apply(null, args)
		});
	}

	off (eventName, callback) {
		if (callback in this._events[eventName]) {
			this._events[eventName].push(callback);
			if (!this._events[eventName].length) {
				delete this._events[eventName];
			}
		} else if (!callback) {
			delete this._events[eventName];
		}
	}

	trigger (eventName, ...args) {
		if (eventName.substr(-2) != '.*') {
			this._triggerExact(eventName, args);
		}

		let eventNameParts = eventName.split('.').slice(0, -1);
		while (eventNameParts.length) {
			this._triggerExact(eventNameParts.join('.')+'.*', args);
			eventNameParts.pop();
		}

		this._triggerExact('*', args);
	}

	_triggerExact (eventName, args) {
		if (!this._events[eventName]) {
			return;
		}

		this._events[eventName].forEach(callback => {
			callback.apply(null, args);
		});
	}

}

module.exports = Events;