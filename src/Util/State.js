
const Events = require('./Events');

class State extends Events {

	constructor() {
		super();

		this.data = {};
		this.definition = {};
	}

	define(key, value, name) {
		if (!(key in this.data) && (key in this)) {
			throw new Error(`Invalid key "${key}"; conflicts with reserved name`);
		}

		if (!(key in this.data)) {
			this.data[key] = value;

			Object.defineProperty(this, key, {
				get: this.__get.bind(this, key),
				set: this.__set.bind(this, key),
			});
		}

		this.definition[this._getKeyValueHash(key, value)] = name;
	}
	name(key) {
		return this._getKeyValueName(key, this[key]);
	}

	__get(key) {
		if (key in this.data) {
			return this.data[key];
		} else {
			return null;
		}
	}
	__set(key, value) {
		if (!this.__has(key, value)) {
			throw new Error(`Can not set undefined value "${value}" for '${key}.`);
		} else if (this.data[key] === value) {
			return;
		}

		this.data[key] = value;
		this.trigger(key, key, value);
	}
	__has(key, value) {
		return (this._getKeyValueHash(key, value) in this.definition);
	}

	_getKeyValueHash(key, value) {
		return [key, value].join('#');
	}
	_getKeyValueName(key, value) {
		let hash = this._getKeyValueHash(key, value);

		if (hash in this.definition) {
			return this.definition[hash];
		} else {
			return null;
		}
	}

}

module.exports = State;
