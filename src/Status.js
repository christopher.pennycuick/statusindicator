const Events = require('./Util/Events');
const Activity = require("./Activity");
const {Chat} = require("./Chat");
const Led = require("./Led");

// config.write('led.brightness', 0.2);
// config.write('mattermost.url', 'https://mm.codebots.com');
// config.write('mattermost.token', '9y4bd71x8tfbie1ut318f6nw5r');

class Status extends Events {

	constructor(config, activity, chat, led, app) {
		super();

		this._config = config;
		this._activity = activity;
		this._chat = chat;
		this._led = led;

		this._activity.state.on('*', this.updateLed.bind(this));
		this._chat.state.on('*', this.updateLed.bind(this));
		// this._chat.state.on(Chat.STATE_CONNECTION, this.updateLed.bind(this));
		// this._chat.state.on(Chat.STATE_STATUS, this.updateLed.bind(this));
	}

	updateLed() {
		let isActive = (this._activity.state[Activity.STATE_ACTIVE] == Activity.ACTIVE_YES);
		let isAtWork = (this._activity.state[Activity.STATE_ATWORK] == Activity.ATWORK_YES);

		let chatStatus = this._chat.state[Chat.STATE_STATUS];

		if (!isActive && !isAtWork) {
			this._applyLed('#000');
		} else if (chatStatus == Chat.STATUS_OFFLINE) {
			this._applyLed('#000');
		} else if (this._chat.state[Chat.STATE_CONNECTION] == Chat.CONNECTION_DISCONNECTED) {
			this._applyLed('#00F');
		} else if (!isActive) {
			this._applyLed('#FA0');
		} else if (chatStatus == Chat.STATUS_DND) {
			this._applyLed('#F00');
		} else if (chatStatus == Chat.STATUS_AWAY) {
			this._applyLed('#FA0');
		} else if (chatStatus == Chat.STATUS_ONLINE) {
			this._applyLed('#0F0');
		} else {
			this._applyLed('#000');
			// console.trace('_applyLed', this._chat.state.data, this._activity.state.data);
		}
	}
	_applyLed (...hexs) {
		this._led.morphMulti(hexs, 500);
	}

}

// Status.EVENT_TIME = 'time.*';
// Status.EVENT_TIME_WORK = 'time.work.*';
// Status.EVENT_TIME_WORK_START = 'time.work.start';
// Status.EVENT_TIME_WORK_END = 'time.work.end';

// Status.EVENT_AWAY = 'away.*';
// Status.EVENT_AWAY_ENABLED = 'away.enabled';
// Status.EVENT_AWAY_DISABLED = 'away.disabled';

// Status.EVENT_STATUS = 'status.*';
// Status.EVENT_STATUS_ONLINE = 'status.online';
// Status.EVENT_STATUS_AWAY = 'status.away';
// Status.EVENT_STATUS_DND = 'status.dnd';
// Status.EVENT_STATUS_OFFLINE = 'status.offline';

module.exports = Status;