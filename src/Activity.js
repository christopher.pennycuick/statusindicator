const desktopIdle = require("desktop-idle");

const Events = require('./Util/Events');
const State = require('./Util/State');
const Config = require("./Config");

class Activity extends Events {

	constructor(config) {
		super();

		this._config = config;

		let worktime = config.read('work_time');
		this._worktime_away = worktime.away;
		this._worktime_start = this._convertTimeStringToMinutesPastMidnight(worktime.start);
		this._worktime_end = this._convertTimeStringToMinutesPastMidnight(worktime.end);

		this._active = null;
		this._withinWorkHours = null;

		this.state = new State();
		this.state.define(Activity.STATE_ACTIVE, Activity.ACTIVE_YES, 'Active');
		this.state.define(Activity.STATE_ACTIVE, Activity.ACTIVE_NO, 'Inactive');
		this.state.define(Activity.STATE_ATWORK, Activity.ATWORK_YES, 'At Work');
		this.state.define(Activity.STATE_ATWORK, Activity.ATWORK_NO, 'Outside Work Hours');
	
		setInterval(this._onMonitoringInterval.bind(this), Activity.POLL_SECONDS * 1000);

		config.on(Config.EVENT_UPDATE('work_time.*'), this._onConfigChange.bind(this));
	}

	active() {
		return this._active;
	}
	atWork() {
		console.log('atWork', this._withinWorkHours, this._isAtWork());
		return this.active() || this._withinWorkHours;
	}

	_onConfigChange(path, value) {
		let worktime = this._config.read('work_time');
		this._worktime_away = worktime.away;
		this._worktime_start = this._convertTimeStringToMinutesPastMidnight(worktime.start);
		this._worktime_end = this._convertTimeStringToMinutesPastMidnight(worktime.end);
	}
	_onMonitoringInterval() {
		this.state[Activity.STATE_ACTIVE] = (this._isActive() ? Activity.ACTIVE_YES : Activity.ACTIVE_NO);
		this.state[Activity.STATE_ATWORK] = (this._isAtWork() ? Activity.ATWORK_YES : Activity.ATWORK_NO);
	}
	_isActive() {
		return (desktopIdle.getIdleTime() < this._worktime_away);
	}
	_isAtWork() {
		let date = new Date();
		let minutesPastMidnight = (date.getHours() * 60) + date.getMinutes();
		return (minutesPastMidnight >= this._worktime_start && minutesPastMidnight <= this._worktime_end);
	}

	_convertTimeStringToMinutesPastMidnight(time) {
		let [hour, minute] = time.split(':');
		return (parseInt(hour) * 60) + parseInt(minute);
	}
}

Activity.POLL_SECONDS = 1;

// Activity.STATE_ACTIVITY = 'activity';
Activity.STATE_ACTIVE = 'active';
Activity.STATE_ATWORK = 'atwork';

Activity.ACTIVE_YES = 'yes';
Activity.ACTIVE_NO = 'no';

Activity.ATWORK_YES = 'yes';
Activity.ATWORK_NO = 'no';

module.exports = Activity;