const {client: WebSocketClient, connection: WebSocketConnection} = require('websocket');

const Events = require('../Util/Events');

class MatterMost extends Events {

	constructor(url, token) {
		super();

		this.userId = null;

		this._url = url;
		this._token = token;

		this._seq = 1;
		this._client = null;
		this._connection = null;
		this._responsePromises = {};
		this._reconnectTimeout = null;

		this._url = this._getUrl();
		this._client = new WebSocketClient();
	}
	connect() {
		if (!this._url || !this._token) {
			throw new Error('Undefined Mattermost URL and/or Token.');
		}

		if (this.connected()) {
			this._connection.close(WebSocketConnection.CLOSE_REASON_NORMAL, 'Reconnecting...');
		}

		this._connect();
	}
	disconnect() {
		if (this.connected()) {
			this._connection.close(WebSocketConnection.CLOSE_REASON_NORMAL);
		}

		this._connection = null;
	}

	connected() {
		return this._connection && this._connection.connected;
	}
	authenticated() {
		return this.connected() && (this.userId !== null);
	}

	send(action, data) {
		if (!this.connected()) {
			return Promise.reject('Websocket not connected.');
		}

		return new Promise(resolve => {
			this._responsePromises[this._seq] = resolve;

			this._connection.send(JSON.stringify({
				seq: this._seq,
				action: action,
				data: data,
			}));

			this._seq += 1;
		});
	}

	_getUrl() {
		let mattermostDomain = (this._url || 'mattermost.com')
			.replace(/^https?:\/\//, '') // Remove leading https?
			.replace(/\/.*$/, ''); // Remove everything after the first slash

		return `wss://${mattermostDomain}/api/v4/websocket`;
	}
	_connect() {
		if (!this._url || !this._token) {
			return;
		}

		new Promise(resolve => {
			this._client.once('connect', connection => {
				this._connection = connection;
				this.trigger(MatterMost.EVENT_CONNECTION_CONNECTED);
				resolve();
			});
			this._client.once('connectFailed', () => {
				this._retryConnect();
			});

			this._client.connect(this._url);
		}).then(() => {
			this._setup();
			this._authenticate();
		});
	}
	_retryConnect() {
		console.log('WS', 'Retry Connection...');
		setTimeout(this._connect.bind(this), MatterMost.TIMEOUT_RETRY_SECONDS * 1000);
	}
	_setup() {
		this._connection.on('error', error => {
			console.error('WS Error', error);
			this._onDisconnect();
		});
		this._connection.on('close', () => {
			this._onDisconnect();
		});
		this._connection.on('message', message => {
			this._onMessage(message);
		});
	}
	_onDisconnect() {
		this.userId = null;

		if (this._connection) {
			this._retryConnect();
		}

		this._connection = null;
		this.trigger(MatterMost.EVENT_CONNECTION_DISCONNECTED);
	}
	_authenticate() {
		this.send('authentication_challenge', { token: this._token });
	}
	_onMessage(message) {
		if (message.type !== 'utf8') {
			console.error("WS: Error: Unknown Message Type", message);
			return;
		}

		const data = JSON.parse(message.utf8Data);

		if ('seq_reply' in data) {
			this._onMessageHandleMessageReply(data);
		} else if ('event' in data) {
			this._onMessageHandleEvent(data);
		} else {
			console.error("WS: Error: Unknown Message Type", message);
		}
	}
	_onMessageHandleMessageReply(data) {
		const seq = data.seq_reply;

		if (!(seq in this._responsePromises)) {
			console.error("WS: Error: Message Reply Not Expected", data);
			return;
		}

		this._responsePromises[seq].call(this, data);
	}
	_onMessageHandleEvent(payload) {
		this._seq = Math.max(this._seq, payload.seq);

		if (payload.event == MatterMost.MESSAGE_EVENT_HELLO) {
			this.userId = payload.broadcast.user_id;
			this.trigger(MatterMost.EVENT_CONNECTION_AUTHENTICATED, this.userId);
		} else {
			this.trigger(MatterMost.EVENT_MESSAGE(payload.event), payload);
		}
	}

};

MatterMost.TIMEOUT_RETRY_SECONDS = 10;

MatterMost.STATUS_ONLINE = 'online';
MatterMost.STATUS_AWAY = 'away';
MatterMost.STATUS_DND = 'dnd';
MatterMost.STATUS_OFFLINE = 'offline';

MatterMost.MESSAGE_ACTION_GET_STATUSES_BY_IDS = 'get_statuses_by_ids'; // data: {user_ids: [userId]}

MatterMost.MESSAGE_EVENT_HELLO = 'hello';
MatterMost.MESSAGE_EVENT_STATUS_CHANGE = 'status_change';

MatterMost.EVENT_CONNECTION = 'status.change.*';
MatterMost.EVENT_CONNECTION_AUTHENTICATED = 'status.change.authenticated';
MatterMost.EVENT_CONNECTION_CONNECTED = 'status.change.connected';
MatterMost.EVENT_CONNECTION_DISCONNECTED = 'status.change.disconnected';
MatterMost.EVENT_MESSAGE = (messageEvent) => {
	return 'message.'+(messageEvent || '*');
}

module.exports = MatterMost;
