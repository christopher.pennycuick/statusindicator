const electron = require("electron");
const fs = require("fs");

const Events = require('./Util/Events');

class Config extends Events {

	constructor() {
		super();

		this._path = (electron.app || electron.remote.app).getPath('userData')+'/preferences.config';
		console.log('Config', 'path', this._path);
		this.data = this._load();
	}

	_load() {
		var data = {};
		var dataString;

		try {
			dataString = fs.readFileSync(this._path);
		} catch (e) {
			if (e.code == 'ENOENT') {
				console.log('Config', 'Preferences file not found.');
			} else {
				throw e;
			}
		}

		try {
			data = JSON.parse(dataString);
		} catch (e) {
			if (e.name == 'SyntaxError') {
				console.log('Config', 'Failed to read preferences file.', e);
				console.log(dataString);
			} else {
				throw e;
			}
		}

		for (let v in Config.migrations) {
			if ((parseInt(data._version) || 0) < v) {
				data = Config.migrations[v](data);
				data._version = v;
			}
		}

		this._store(data);

		return data;
	}
	_store(data) {
		fs.writeFile(this._path, JSON.stringify(data, null, 2), {}, (err) => {
			if (err) {
				throw err;
			}
		});
	}

	read(path, defaultValue = null) {
		let pathParts = path.split('.');

		var key;
		var data = this.data;
		while (pathParts.length > 1) {
			key = pathParts.shift();
			if (key in data) {
				data = data[key];
			} else {
				return defaultValue;
			}
		}

		key = pathParts[0];
		if (key in data) {
			return data[key];
		} else {
			return defaultValue;
		}
	}

	write(path, value) {
		let pathParts = path.split('.');

		var key;
		var data = this.data;
		while (pathParts.length > 1) {
			key = pathParts.shift();
			if (key in data) {
				data = data[key];
			} else {
				return null;
			}
		}

		key = pathParts[0];
		if (key in data) {
			data[key] = value;
		}

		this._store(this.data);
		this.trigger(Config.EVENT_UPDATE(path), path, value);
	}
	writeAll(config) {
		Object.assign(config, {_version: this.data._version});

		let diffFlat = this._calculateDiff(this.data, config);

		Object.assign(this.data, config);

		this._store(this.data);

		for (let path in diffFlat) {
			this.trigger(Config.EVENT_UPDATE(path), path, diffFlat[path]);
		}
	}

	_calculateDiff(left, right, path = []) {
		let diff = {};

		for (let key in left) {
			if (!(key in right)) {
				continue;
			} else if (typeof left[key] === 'object' && typeof right[key] === 'object') {
				Object.assign(diff, this._calculateDiff(left[key], right[key], path.concat([key])));
			} else if (left[key] != right[key]) {
				diff[path.concat([key]).join('.')] = right[key];
			}
		}

		return diff;
	}
}

Config.migrations = {
	2019030201: function (data) {
		return {
			led: {
				brightness: 0.2,
			},
			mattermost: {
				url: null,
				token: null,
			},
			work_time: {
				away: 5 * 60, // 5min
				start: '07:00',
				end: '18:00',
			},
			features: {
				notify_on_message: false,
			},
		};
	},
	2019042201: function (data) {
		data.features.chat_notify_on_message = data.features.notify_on_message;
		data.chat = {
			provider: 'Provider.None',
			mattermost: data.mattermost,
		};

		delete data.mattermost;
		delete data.features.notify_on_message;

		return data;
	},
};

Config.defaults = {
	led: {
		brightness: 0.2,
	},
	chat: {
		provider: null,
		mattermost: {
			url: null,
			token: null,
		},
	},
	work_time: {
		away: 5 * 60, // 5min
		start: '07:00',
		end: '18:00',
	},
	features: {
		chat_notify_on_message: false,
	},
};

Config.EVENT_UPDATE = (path) => { 
	return 'update.'+(path || '*'); 
};

module.exports = Config;
