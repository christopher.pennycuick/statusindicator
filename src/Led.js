const BlinkStick = require('../BlinkStickNode/blinkstick');

const Config = require('./Config');
const Events = require('./Util/Events');
const State = require('./Util/State');

class Led extends Events {

	constructor(config) {
		super();

		this._config = config;

		this._type = Led.TYPES[1];

		this._led = null;
		this._connectedInterval;
		this._disconnectedInterval;

		this._brightness = parseFloat(config.read('led.brightness', 1));
		config.on(Config.EVENT_UPDATE('led.brightness'), (path, brightness) => {
			this._brightness = parseFloat(brightness);
			if (!this._animation.isPlaying()) {
				this._setRGB(this.rgb);
			}
		});

		this._animation = new LedAnimation(this);

		this.rgb = new Array(this._type.led_count * 3);
		for (let i = 0; i < this.rgb.length; i++) {
			this.rgb[i] = 0;
		}

		this.state = new State();
		this.state.define(Led.STATE_CONNECTION, Led.CONNECTION_DISCONNECTED, 'Disconnected');
		this.state.define(Led.STATE_CONNECTION, Led.CONNECTION_CONNECTED, 'Connected');

		this._monitor();
	}

	_monitor() {
		Promise.resolve()
			.then(this._onConnect.bind(this))
			.then(this._onDisconnect.bind(this))
			.then(this._monitor.bind(this));
	}
	_onConnect() {
		clearInterval(this._connectedInterval);

		return Promise.resolve()
			.then(this._findLED.bind(this))
			.then(found => {
				if (found) {
					return Promise.resolve()
				}

				return new Promise(resolve => {
					this._connectedInterval = setInterval(() => {
						this._findLED()
							.then(found => {
								if (found) {
									clearInterval(this._connectedInterval);
									resolve();
								}
							});
					}, 5 * 1000); // 5s
				});
			})
			.then(() => {
				this.state[Led.STATE_CONNECTION] = Led.CONNECTION_CONNECTED;
			});
	}
	_findLED() {
		this.setAll('#000');

		this._led = BlinkStick.findFirst();

		if (this._led == null) {
			return Promise.resolve(false);
		}

		return Promise.all([
			new Promise(resolve => { this._led.getDescription((err, data) => { resolve(data); }) }),
			new Promise(resolve => { this._led.getSerial((err, serial) => { resolve(serial); }) }),
		]).then(() => {
			return Promise.resolve(true);
		});
	}
	_onDisconnect() {
		clearInterval(this._disconnectedInterval);

		return new Promise(resolve => {
			try {
				this._disconnectedInterval = setInterval(() => {
					this._led.getSerial((err, serial) => {
						BlinkStick.findBySerial(serial, (led) => {
							if (!led) {
								this._led = null;
								clearInterval(this._disconnectedInterval);
								resolve();
							}
						});
					});
				}, 5 * 1000); // 5s
			} catch (e) {
				console.error(e);

				this._led = null;
				clearInterval(this._disconnectedInterval);
				resolve();
			}
		}).then(() => {
			this.state[Led.STATE_CONNECTION] = Led.CONNECTION_DISCONNECTED;
		});
	}

	close() {
		this.setAll("000");
	}

	connected() {
		return (this._led !== null);
	}

	setAll(hex) {
		this.setMulti([hex]);
	}
	setMulti(hexs) {
		this._setRGB(this._hexsToRGBTarget(hexs));
	}

	morphAll(hex, duration = 1000, hold = 50) {
		return this.morphMulti([hex], duration, hold);
	}
	morphMulti(hexs, duration = 1000, hold = 50) {
		const steps = Math.ceil(duration / hold);

		let animation = this.createAnimation().setRepeat(steps);

		const rgb_target = this._hexsToRGBTarget(hexs);

		animation.addStep(step => {
			const remainingFraction = Math.min(1, 1 / (steps - step));
			let rgb_next = this.rgb.slice(0);

			let diff;
			for (let i = 0; i < rgb_next.length; i++) {
				diff = rgb_target[i] - rgb_next[i];
				rgb_next[i] += Math.round(diff * remainingFraction);
			}

			return rgb_next;
		}, hold);

		return this.animate(animation);
	}

	flashAll(hex, repeat = 0, hold = 100) {
		return this.flashMulti([hex], repeat, hold);
	}
	flashMulti(hexs, repeat = 0, hold = 100) {
		let animation = this.createAnimation().setRepeat(repeat);

		const rgb_off = this._hexsToRGBTarget(['#000']); // clone
		const rgb_flash = this._hexsToRGBTarget(hexs);

		animation.addStep(rgb_flash, hold);

		if (repeat > 0) {
			animation.addStep(rgb_off, hold);
		}

		return this.animate(animation, true);
	}

	createAnimation() {
		return new LedAnimationBuilder();
	}
	animate(animation, reset = false) {
		this._animation.reset(animation);
		return this._animation.start(reset);
	}

	_setRGB(rgb) {
		if (!rgb || !rgb.length) {
			return;
		}
		
		this.trigger(Led.EVENT_CHANGE_COLOR, rgb.slice());

		if (this._led == null) {
			return;
		}

		this.rgb = rgb.slice();

		for (let i = 0; i < rgb.length; i++) {
			rgb[i] *= this._brightness;
		}

		this._led.setColors(0, rgb);
	}
	_hexsToRGBTarget(hexs) {
		const rgbs = this._hexsToRGBs(hexs);
		let rgb_target = this.rgb.slice(0); // clone

		let pixel;
		let x2 = (hexs.length <= (this._type.led_count / 2)) ? 2 : 1;
		for (let repeat = 0; repeat < this._type.led_count; repeat++) {
			for (let i = 0; i < hexs.length; i++) {
				if (hexs[i] == null) {
					continue;
				}

				for (let j = 0; j < x2; j++) {
					pixel = repeat * hexs.length * x2 + i * x2 + j;

					if (pixel >= this._type.led_count) {
						return rgb_target;
					}

					rgb_target[pixel * 3 + 0] = rgbs[i][1];
					rgb_target[pixel * 3 + 1] = rgbs[i][0];
					rgb_target[pixel * 3 + 2] = rgbs[i][2];
				}
			}
		}

		return rgb_target;
	}
	_hexsToRGBs(hexs) {
		let rgbs = {};

		let half;
		let hex;

		for (let i = 0; i < hexs.length; i++) {
			hex = hexs[i];

			if (!hex || !hex.length) {
				rgbs[i] = null;
				continue;
			}

			if (hex[0] == '#') {
				hex = hex.substr(1);
			}

			half = (hex.length == 3);

			rgbs[i] = [
				parseInt(half ? hex.substr(0, 1) + hex.substr(0, 1) : hex.substr(0, 2), 16),
				parseInt(half ? hex.substr(1, 1) + hex.substr(1, 1) : hex.substr(2, 2), 16),
				parseInt(half ? hex.substr(2, 1) + hex.substr(2, 1) : hex.substr(4, 2), 16),
			];
		}

		return rgbs;
	}
}

class LedAnimationBuilder {
	constructor() {
		this.steps = [];
		this.repeat = 0;
	}
	setRepeat(repeat) {
		this.repeat = repeat;
		return this;
	}
	addStep(rgb, hold) {
		this.steps.push({
			rgb: rgb,
			hold: hold,
		});

		return this;
	}
}
class LedAnimation {
	constructor(led) {
		this._builder = new LedAnimationBuilder();

		this._step = 0;
		this._stepLength = 0;
		this._timeout = null;
		this._promise = null;

		this._led = led;
		this._rgbOrig = this._led.rgb;
	}
	isPlaying() {
		return (this._promise !== null);
	}
	reset(animation = null) {
		this.stop();

		this._step = 0;
		this._stepLength = 0;

		if (animation) {
			this._builder = animation;
		}
	}
	stop() {
		if (!this.isPlaying()) {
			return;
		}

		clearTimeout(this._timeout)
		this._timeout = null;

		const resolve = this._promise.resolve;
		this._promise = null;

		resolve();
	}
	start(reset = false) {
		this.stop();

		this._rgbOrig = this._led.rgb;
		this._stepLength = this._builder.steps.length * (this._builder.repeat + 1);

		return new Promise(resolve => {
			this._promise = {
				resolve: resolve,
			};

			this._showStep(() => {
				this._timeout = null;
				this._promise = null;

				if (reset) {
					this._led._setRGB(this._rgbOrig);
				} else {
					this._rgbOrig = this._led.rgb;
				}

				resolve();
			});
		});
	}
	restart() {
		this._step = 0;
		this.start();
	}

	_showStep(doneCallback) {
		// If this is the end of the animation
		if (this._step >= this._stepLength) {
			doneCallback();
			return;
		}

		let { rgb, hold } = this._builder.steps[this._step % this._builder.steps.length];

		if (typeof rgb === 'function') {
			rgb = rgb(this._step);
		}

		this._led._setRGB(rgb);
		this._step++;

		this._timeout = setTimeout(() => {
			this._showStep(doneCallback);
		}, hold);
	}
}

Led.TYPES = [
	{
		id: 'Nano',
		name: 'Blinkstick Nano',
		led_count: 2,
	},
	{
		id: 'Square',
		name: 'Blinkstick Square',
		led_count: 8,
	}
];

Led.STATE_CONNECTION = 'connection';

Led.CONNECTION_CONNECTED = 'connected';
Led.CONNECTION_DISCONNECTED = 'disconnected';

Led.EVENT_CHANGE_COLOR = 'change-color';
Led.EVENT_LED_COUNT_CHANGE = 'led-count-change';

module.exports = Led;
