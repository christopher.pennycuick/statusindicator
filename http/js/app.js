const pages = require('./js/pages');
const components = require('./js/components');
const plugins = require('./js/plugins');

const routes = {
	'': pages.Home,
	'#home': pages.Home,
	'#about': pages.About,
	'#preferences': pages.Preferences,

	'#notfound': pages.NotFound,
};

for (const name in pages) {
	const page = pages[name];
	if ('hash' in page) {
		routes['#'+page.hash] = page;
	}
}

for (const name in plugins) {
	if (plugins.hasOwnProperty(name)) {
		Vue.use(plugins[name]);
	}
}

for (const key in components) {
	Vue.component(key, components[key]);
}

window.app = new Vue({
	el: '#app',
	plugins,
	computed: {
		currentPage () {
			return this.$store.App.data.currentPage;
		},
		ViewComponent() {
			return routes[this.currentPage] || routes['#notfound'];
		}
	},
	render (h) { 
		return h(this.ViewComponent); 
	},
});
