
let data = Vue.observable({
	color: [],
});

module.exports = {
	data,
	setColor (color) {
		Vue.set(data, 'color', color);
	},
};
