
let data = Vue.observable({
	version: '0.1.0',
	currentPage: window.location.hash,

	device_connection: 'unknown',
	chat_connection: 'unknown',
	chat_status: 'unknown',
	activity: 'unknown',
});

window.onhashchange = () => {
	data.currentPage = window.location.hash;
};

module.exports = {
	data,
};
