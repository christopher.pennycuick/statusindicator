
let data = Vue.observable({
	preferences: null,
});

module.exports = {
	data,
	setPreferences(preferences) {
		Vue.set(data, 'preferences', preferences);
	},
	save: (function () {
		let saveCallback = [];
	
		let save = () => {
			for (let i in saveCallback) {
				saveCallback[i](data.preferences);
			}
		};
	
		save.listen = (callback) => {
			saveCallback.push(callback);
		};
	
		return save;
	})(),
};
