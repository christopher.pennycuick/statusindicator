module.exports = {
	hash: 'about',
	template: `
<main>
	<app-button-close></app-button-close>
	<article>
		<h1>About</h1>
		<p>This simple app controls your BlinkStick LED and changes it's coluor depending on your chat status (if available) and your activity on the machine.</p>
		<hr />
		<b>Author:</b> Chris Pennycuick<br/>
		<b>Version:</b> {{version}}<br/>
	</article>
</main>
`,
	computed: {
		version() {
			return this.$store.App.data.version;
		}
	}
}