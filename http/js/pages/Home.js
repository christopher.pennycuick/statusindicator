module.exports = {
	hash: 'home',
	template: /* html */`
<main>
	<div class="leds">
		<app-tooltip class="led" direction="down" v-for="n in (led_color.length / 3)" v-bind:style="{ 'background-color': getRGBColor(n) }">
			<template v-slot:text><span style="white-space: nowrap;">LED {{n}}</span></template>
		</app-tooltip>
		<div v-if="chat_enabled" class="led-text">Chat Status {{chat_status}}</div>
	</div>
	<div class="status-icons">
		<app-status :color="device_connection_color" direction="left">
			<span style="white-space: nowrap;">LED {{device_connection}}</span>
		</app-status>
		<app-status :color="activity_color" direction="left">
			<span style="white-space: nowrap;">User {{ activity }}</span>
		</app-status>
	</div>
	<article></article>
</main>
	`,
	computed: {
		led_color () {
			return this.$store.Led.data.color;
		},
		device_connection () {
			return this.$store.App.data.device_connection;
		},
		device_connection_color () {
			switch (this.device_connection) {
				case 'Connected':
					return 'darkgreen';
				default:
					return 'red';
			}
		},
		chat_enabled () {
			return (this.$store.App.data.chat_status != 'Provider.None');
		},
		chat_status () {
			return this.$store.App.data.chat_status;
		},
		activity () {
			return this.$store.App.data.activity;
		},
		activity_color () {
			if (this.activity.match(/^Active/) === null) {
				return 'red';
			} else {
				return 'darkgreen';
			}
		},
	},
	methods: {
		getRGBColor(i) {
			return 'rgb(' + this.led_color[(i - 1) * 3 + 1] + ', ' + this.led_color[(i - 1) * 3 + 0] + ', ' + this.led_color[(i - 1) * 3 + 2] + ')';
		}
	}
};
