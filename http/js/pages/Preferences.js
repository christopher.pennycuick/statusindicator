module.exports = {
	hash: 'preferences',
	template: /* html */`
<main>
	<app-button-close></app-button-close>
	<article>
		<h1>Preferences</h1>
		<section v-if="preferences !== null">
			<fieldset>
				<legend>LED</legend>
				<label>
					<b>Brightness:</b> <span>{{preferences.led.brightness | formatPercent}}</span><br/>
					<input type="range" list="range-brightness" min="0.1" max="1" step="0.1" v-on:change="updateConfig" v-model="preferences.led.brightness">
					
					<datalist id="range-brightness">
						<option value="0.1" label="10%">
						<option value="0.2">
						<option value="0.3">
						<option value="0.4">
						<option value="0.5" label="50%">
						<option value="0.6">
						<option value="0.7">
						<option value="0.8">
						<option value="0.9">
						<option value="1.0" label="100%">
					</datalist>
				</label>
			</fieldset>
			<fieldset>
				<legend>Chat</legend>
				<label>
					<b>Provider:</b>
					<select v-on:change="updateConfig" v-model="preferences.chat.provider">
						<option value="Provider.None">None</option>
						<option value="Provider.MatterMost">MatterMost</option>
					</select>
				</label>
				<div v-if="preferences.chat.provider == 'Provider.MatterMost'">
					<label>
						<b>URL:</b>
						<input type="text" v-on:change="updateConfig" v-model="preferences.chat.mattermost.url">
					</label>
					<br/>
					<label>
						<b>Token:</b>
						<input type="text" v-on:change="updateConfig" v-model="preferences.chat.mattermost.token">
					</label>
					<br/>
					<label>
						<b>Notify on message (currently unsupported):</b>
						<input type="checkbox" v-on:change="updateConfig" v-model="preferences.features.chat_notify_on_message">
					</label>
				</div>
			</fieldset>
			<fieldset>
				<legend>Work Time</legend>
				<label>
					<b>Away (seconds):</b>
					<input type="number" v-on:change="updateConfig" v-model="preferences.work_time.away">
				</label>
				<br/>
				<label>
					<b>Start Time (HH:MM):</b>
					<input type="time" v-on:change="updateConfig" v-model="preferences.work_time.start">
				</label>
				<br/>
				<label>
					<b>End Time (HH:MM):</b>
					<input type="time" v-on:change="updateConfig" v-model="preferences.work_time.end">
				</label>
			</fieldset>
		</section>
	</article>
</main>
`,
	computed: {
		preferences () {
			return this.$store.Preferences.data.preferences;
		}
	},
	methods: {
		updateConfig(event) {
			this.$store.Preferences.save();
		}
	},
	filters: {
		formatPercent(value) {
			return (value * 100) + '%';
		}
	}
}