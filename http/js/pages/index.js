
module.exports = {
	Home: require('./Home'),
	About: require('./About'),
	Preferences: require('./Preferences'),

	NotFound: require('./NotFound'),
};
