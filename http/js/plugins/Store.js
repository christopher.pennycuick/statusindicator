const stores = require('../store');

let data = {};

for (const name in stores) {
	if (stores.hasOwnProperty(name)) {
		data[name] = stores[name];
	}
}

module.exports = {
	install(Vue, options) {
		Vue.prototype.$store = data;
	},
	map(map) {
		for (let name in map) {
			if (map.hasOwnProperty(name)) {
				let value = map[name];
				map[name] = function () {
					return value;
				};
			}
		}

		return map;
	}
};
