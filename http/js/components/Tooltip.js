
let validDirections = ['left', 'right', 'up', 'down'];

module.exports = {
	props: ['text', 'direction'],
	template: /* html */`
<span class="tooltip">
	<span :class="['tooltip--text', direction_class]"><slot name="text"></slot></span>
	<slot></slot>
</span>
	`,
	computed: {
		direction_class() {
			return 'tooltip--direction-' + this.checkDirection(this.direction);
		},
	},
	methods: {
		checkDirection(value) {
			if (validDirections.indexOf(value) >= 0) {
				return value;
			} else {
				return 'left';
			}
		},
	}
};
