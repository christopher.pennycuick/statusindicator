module.exports = {
	props: ['color', 'direction'],
	template: `
<app-tooltip :direction="direction">
	<div class="status" :style="{'background-color': color}"></div>
	<template v-slot:text><slot></slot></template>
</app-tooltip>
	`,
}