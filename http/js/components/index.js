
module.exports = {
	'app-status': require('./Status'),
	'app-tooltip': require('./Tooltip'),
	'app-button-close': require('./ButtonClose'),
};
